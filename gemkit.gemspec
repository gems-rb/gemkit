Gem::Specification.new do |gem|
	gem.name         = 'gemkit'
	gem.version='0.0.4'
	gem.summary		= 'ruby gem to support rake tasks for ruby gems'
	gem.description	= 'supports common rake development tasks for ruby gems'
	gem.authors		= ["Lou Parslow"]
	gem.email			= 'lou.parslow@gmail.com'
	gem.homepage		= 'http://rubygems.org/gems/gemkit'
    gem.required_ruby_version = '>= 2.0.0'
	gem.files         = Dir["LICENSE","README.md","{lib}/**/*.rb"]
	gem.licenses      = ['MIT']
	gem.add_runtime_dependency 'raykit', '~>0.0'
end