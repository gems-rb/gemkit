require 'raykit'


desc 'build the gem'
task :build do
    puts Rainbow(":build").blue.bright
    PROJECT.run([
        "bundle",
        "gem build raykit.gemspec"])
end

