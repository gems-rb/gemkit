require 'raykit'
NAME='gemkit'

desc 'build the gem'
task :build do
    puts Rainbow(":build").blue.bright
    PROJECT.run([
        "bundle",
        "gem build #{PROJECT.name}.gemspec"])
end

task :publish do
    gem="#{PROJECT.name}-#{PROJECT.version}.gem"
    PROJECT.run("gem push #{gem}",false)
end

task :default => [:build]